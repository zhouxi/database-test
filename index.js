const {app, BrowserWindow, ipcMain} = require('electron')
const path = require('path')
const url = require('url')
const sqlite3 = require('sqlite3')
const fs = require('fs')
// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let win

let dev = process.argv.includes('--dev')

var knex = require("knex")({
  client: "sqlite3",
  connection:{
	   filename:"./user.db"
  }
});

//session variable
global.sesObject = {
  user: "",
  password: ""
}

ipcMain.on("login", function (evt, name, password) {
  knex('USERS').where({
    USER_NAME: name,
    PASSWORD: password
  }).select('*')
  .then(function(rows){
    if (win && win.webContents) {
      win.webContents.send("resultSent", rows);
    }
  })
});


ipcMain.on("register", function (evt, name, username, password, department) {
  knex('USERS').where({
    NAME: name,
    USER_NAME:username,
    PASSWORD: password,
    DEPARTMENT: department
  }).select('*')
  .then(rows => {
    if(rows.length > 0 && win && win.webContents) {
      win.webContents.send("resultSent", "用户已存在");
    } else {
      knex.insert({
        NAME: name,
        USER_NAME: username,
        PASSWORD: password, 
        DEPARTMENT: department
      }).into("USERS")
      .then(() => {
        if (win && win.webContents) {
          win.webContents.send("resultSent", "");
        }
      })
    }
  })
});

function createWindow () {
  // Create the browser window.
  win = new BrowserWindow({width: 800, height: 600})

  win.loadURL(url.format({
    pathname: path.join(__dirname, './pages/loginPage.html'),
    protocol: 'file:',
    slashes: true
  }))

  //session
  var session = win.webContents.session;

  //cookies 
  session.cookies.get({}, (error, cookies) => {
    if (cookies.length > 0) {
    	sesObject.user = cookies[0].value;
    	sesObject.password = cookies[1].value;
    	win.loadURL(`file://${__dirname}/pages/mainPage.html`)
    } else {
      win.loadURL(`file://${__dirname}/pages/loginPage.html`)
    }
  })
  
  // Open the DevTools.
  if (dev) {
    win.webContents.openDevTools();
  }

  // Emitted when the window is closed.
  win.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    win = null;
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win === null) {
    createWindow()
  }
})