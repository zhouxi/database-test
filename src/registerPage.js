const remote = require('electron').remote
let win = remote.getCurrentWindow();
const ipc = require('electron').ipcRenderer;

$("#register").click(() => {
	var user = document.getElementById("user-name").value;
	var users={
		"name":document.getElementById("full-name").value,
		"user_name":document.getElementById("user-name").value,
		"password":document.getElementById("pass-word").value,
		"department":document.getElementById("department").value
	}
	
	if(users.user_name.length < 2) {
		alert("username must have at least 2 characters");
		return;
	}
	if(users.password.length < 6) {
		alert("password must have at least 6 characters");
		return;
	}
	
	ipc.send("register", users.name, users.user_name, users.password, users.department)
})

ipc.on("resultSent", function(evt, result){
  if(result.length > 0) {
    alert(result)
  } else {
    var user_name = document.getElementById("user-name").value
    remote.getGlobal('sesObject').user = user_name;
    win.loadURL(`file://${__dirname}/../pages/mainPage.html`)
  }
});

$("#cancel").click(() => {
	win.loadURL(`file://${__dirname}/../pages/loginPage.html`);
})
