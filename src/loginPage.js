const remote = require('electron').remote
const session = remote.session;
const electron = require('electron')
const ipc = require('electron').ipcRenderer;

let win = remote.getCurrentWindow()

$("#login").click(() => {
	var message = '';
	var users={
	    "user_name": document.getElementById("user-name").value,
	    "pass_word": document.getElementById("pass-word").value
	}
  ipc.send("login", users.user_name, users.pass_word)
})

ipc.on("resultSent", function(evt, result) {
  if(result.length > 0) {
    console.log(result)
    remote.getGlobal('sesObject').user = document.getElementById("user-name").value;
    remote.getGlobal('sesObject').password = document.getElementById("pass-word").value;
    if(document.getElementById("check-box").checked == true) {
      saveNameAndPassword();
    }
    win.loadURL(`file://${__dirname}/mainPage.html`)
  } else {
    console.error("login failed");
  }
});

let saveNameAndPassword = () => {
	let name = document.getElementById("user-name").value;
	let password = document.getElementById("pass-word").value;
	setCookie('name', name);
	setCookie('password', password);
};

let setCookie = (name, value) => {
	let Days = 30;
	let exp = new Date();
	let date = Math.round(exp.getTime() / 1000) + Days * 24 * 60 * 60;
	const cookie = {
	   url: "http://localhost",
	   name: name,
	   value: value,
	   expirationDate: date	
	};
	session.defaultSession.cookies.set(cookie, (error) => {
	   if (error) console.error(error);
	   else console.log("cookies set");    
	});
};

let getCookies = () => {
  session.defaultSession.cookies.get({ url: "http://localhost" }, function (error, cookies) {
    if (cookies.length > 0) {
      let name = document.getElementById('name');
      name.value = cookies[0].value;
      let password = document.getElementById('password');
      password.value = cookies[1].value;
    }
  });
};

let clearCookie = () => {
	session.defaultSession.clearStorageData({
		origin: "http://localhost",
		storage: ['cookies']
	}, function (error) {
		if(error) console.error(error);
	})
};
